def check(a, b):
    try:
        a = int(a)
        b = int(b)
    except ValueError:
        return False
    return True


def counter(a, b):
    k = 0
    for i in range(a, b + 1):
        print(i, bin(i)[2:])  # слава отладке
        if bin(i)[2:].count('0') == 1:
            k += 1
    return k


def main():
    a, b = input("Введите 2 целых числа \n").split()
    if check(a, b):
        a = int(a)
        b = int(b)
        print(counter(a, b))
    else:
        print("Ошибка ввода. Прога завершитсяи ответа вы не получите")


if __name__ == "__main__":
    main()
