import lab4


def tests_counter():
    assert lab4.counter(5, 10) == 2
    assert lab4.counter(0, 10) == 4
    assert lab4.counter(10, 50) == 8
    assert lab4.counter(-5, 5) == 5
    assert lab4.counter(0, 0) == 1
    assert lab4.counter(1, 1) == 0


def test_check():
    assert lab4.check('2', '4') is True
    assert lab4.check('1', '-4') is True
    assert lab4.check('-2', '4') is True
    assert lab4.check('-2', '-4') is True
    assert lab4.check('STROKA', '4') is False
    assert lab4.check('2', 'kto?') is False
    assert lab4.check('STROKA', 'kto?') is False

