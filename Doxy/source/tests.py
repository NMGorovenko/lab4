import lab4


def tests_counter_1():
    """
    Фукнция проверки функции counter() в модуле lab4
    В ней рассматриваются следущие тестовые случаи:
        1) Первый аргумент меньше второго
        2) Второй аргумент меньше первого
        3) Один из аргументов -отрицательным числом
        4) Оба аргумента равны те, где поулчается нулевой отрезок(см документакцию функции counter())
    """
    assert lab4.counter(5, 10) == 2
    assert lab4.counter(10, 5) == 2
    assert lab4.counter(0, 10) == 4
    assert lab4.counter(10, 50) == 8
    assert lab4.counter(-5, 5) == 5
    assert lab4.counter(0, 0) == 1
    assert lab4.counter(1, 1) == 0


def test_check():
    """
    Функция проверки функции check() в модуле lab4
    Функция check() должна проверять, возможен ли переход двух аргументов из типа str в тип int
    В ней рассматриваются следущие случаи:
        1) Обе строки содержат положительные числа
        2) Один из аргументов - отрицательное число
        3) Оба аргумента - отрицательные числа
        4) Один из аргументов не переводится в тип int
        5) Оба аргумента не переводятся в тип int
    """
    assert lab4.check('2', '4') is True
    assert lab4.check('1', '-4') is True
    assert lab4.check('-2', '4') is True
    assert lab4.check('-2', '-4') is True
    assert lab4.check('STROKA', '4') is False
    assert lab4.check('2', 'kto?') is False
    assert lab4.check('STROKA', 'kto?') is False
